function createCard(name, description, pictureUrl, start, end, location) {
    return `
    <div class="col">
      <div class="card" style="box-shadow: 5px 5px 5px 0px rgba(0,0,0,0.75); margin: 20px;">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
          <p class="card-text">${description}</p>
        </div>
        <div class="card-footer">
            ${start} - ${end}
      </div>
    </div>
    `;
  }

function errorAlert(e) {
    return `
        <div class="alert alert-danger alert-dismissible fade show" role="alert">
            ${e}
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                <span aria-hidden="true">&times;</span>
        </div>
    `;
}


window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    try {
      const response = await fetch(url);

      if (!response.ok) {
        // Figure out what to do when the response is bad
        const e = "An error has occured!"
        console.error(e)
        const html = errorAlert(e);
        const column = document.querySelector('.row');
        column.innerHTML += html;
      } else {
        const data = await response.json();

        for (let conference of data.conferences) {
            const detailURL = `http://localhost:8000${conference.href}`;
            const detailResponse = await fetch(detailURL);
            if (detailResponse.ok) {
                const details = await detailResponse.json();
                const title = details.conference.name;
                const description = details.conference.description;
                const pictureUrl = details.conference.location.picture_url;
                const startDate = new Date(details.conference.starts);
                const start = `${startDate.getMonth()}/${startDate.getDate()}/${startDate.getFullYear()}`;
                const endDate = new Date(details.conference.ends);
                const end = `${endDate.getMonth()}/${endDate.getDate()}/${endDate.getFullYear()}`;
                const location = details.conference.location.name;
                const html = createCard(title, description, pictureUrl, start, end, location);
                const column = document.querySelector('.row');
                column.innerHTML += html;

            }
        }

      }
    } catch (e) {
      // Figure out what to do if an error is raised
        console.error(e)
        const html = errorAlert(e);
        const error = document.querySelector('.row');
        error.innerHTML += html;
    }

  });
